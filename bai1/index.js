function tinhTienLuong() {
  var tienLuong = document.getElementById("tien-luong").value * 1;
  var soNgayCong = document.getElementById("ngay-cong").value * 1;
  var tinhTien = tienLuong * soNgayCong;

  document.getElementById(
    "result"
  ).innerHTML = `<h5>Tổng tiền lương là: ${tinhTien} vnđ</h5>`;
}

function tinhTrungBinh() {
  var so1 = document.getElementById("so-1").value * 1;
  var so2 = document.getElementById("so-2").value * 1;
  var so3 = document.getElementById("so-3").value * 1;
  var so4 = document.getElementById("so-4").value * 1;
  var so5 = document.getElementById("so-5").value * 1;
  var tinhSoTrungBinh = (so1 + so2 + so3 + so4 + so5) / 5;
  document.getElementById(
    "result-trung-binh-cong"
  ).innerHTML = `<h5>Trung bình cộng của 2 số là: ${tinhSoTrungBinh}</h5>`;
}

function quyDoiTien() {
  const tienUSA = 23500;
  var soTienUSANhap = document.getElementById("tien-usa").value * 1;
  var quyDoi = soTienUSANhap * tienUSA;
  document.getElementById(
    "result-quy-doi"
  ).innerHTML = `<h5>Số tiền VNĐ là: ${quyDoi}</h5>`;
}

function chuViVaDienTich() {
  var chieuRong = document.getElementById("chieu-rong").value * 1;
  var chieuDai = document.getElementById("chieu-dai").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  var dienTich = chieuDai * chieuRong;
  document.getElementById(
    "result-chuvi-dientich"
  ).innerHTML = ` <h5>Chu vi là: ${chuVi}     , Diện tích là : ${dienTich}</h5> `;
}

function kySo() {
  var nhapKySo = document.getElementById("ky-so").value * 1;
  var hangChuc = Math.floor(nhapKySo / 10);
  var hangDonVi = nhapKySo % 10;

  var tongKySo = hangChuc + hangDonVi;
  document.getElementById(
    "result-ky-so"
  ).innerHTML = `<h5>Tổng 2 ký số là : ${tongKySo} </h5>`;
}
